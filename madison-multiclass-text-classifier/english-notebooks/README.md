![Madison](/img/madison-logo.png) 

# MADISON DATA - Multiclass Text Classifier (Sentiment y Topics)

*(versión en Español [aquí](/madison-multiclass-text-classifier/english-notebooks/README.md))*

## Introduction 
The natural language processing area of the **Madison Data** team has developed a multiclass text classifier algorithm and packaged it so that it can be deployed in the infrastructure provided by [AWS Sagemaker](https://aws.amazon.com/en/sagemaker/).

This algorithm models the labeling of **customer experience feedback** in Spanish as it is one of Madison MK's main areas of expertise. However, the algorithm can be successfully trained to classify texts from other domains and in other languages as it can be checked with the sample datasets.

## How to use
The integration process has been designed so that it can be used by development teams without advanced knowledge of Data Science or Machine Learning.

 It is only necessary to subscribe from the [Madison Multiclass Text Classifier](https://aws.amazon.com/marketplace/pp/prodview-ywbow7v3bdcvo?ref_=srh_res_product_title) product page in the Algorithms and Models section of [AWS Marketplace](https://aws.amazon.com/marketplace/)
 
 Once the subscription is complete, the product can be used following the steps detailed in the *notebooks* distributed with this repository for two use cases:

   - [Classification of texts into categories](</madison-multiclass-text-classifier/spanish-notebooks/news/Algorithm Test - Madison Multiclass Text Classifier - News Classification.ipynb?viewer=nbviewer>): The *notebook* provides the steps to complete the training of a text classification model in thematic categories. The example uses the news data set described in the next section.
   - [Extracting the sentiment implicit in the text](</madison-multiclass-text-classifier/spanish-notebooks/imdb-sentiment/Algorithm Test - Madison Multiclass Text Classifier - Sentiment.ipynb?viewer=nbviewer>) The *notebook* provided contains the steps to complete the training of a model that extracts the implicit sentiment from a text. The example uses the movie review dataset described in the next section.

In both use cases, the result will be a model trained to predict classes for new texts and that is deployed in a AWS Sagemaker *end-point* that can be consumed by any other software through *REST* requests.

## Datasets
In order to help with the deployment, we provide two sample datasets for two different use cases. One with **news in Spanish** and another with **movie reviews in English**.

- [News in Spanish](/data/madison-multiclass-text-classifier/madison-news): The dataset contains news items published on the Madison MK blog.
- [IMDB movie reviews](https://www.kaggle.com/columbine/imdb-dataset-sentiment-analysis-in-csv-format): The dataset is splitted into 3 files (Training, Test and Validation) with a combined size of over 49,000 reviews that are labeled with either positive sentiment ('1') or negative sentiment ('0')

However, we encourage you to use your own datasets to train the algorithm and generate fully customized models to your use cases.

## Licenses
The news dataset in Spanish language is distributed under [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/deed.es) and it has been generated by Madison MK.

The IMDB Movie Review Dataset is distributed with the [World Bank Dataset Terms of Use](https://www.worldbank.org/en/about/legal/terms-of-use-for-datasets) and we found it in this [Kaggle sentiment analysis competition](https://www.kaggle.com/columbine/imdb-dataset-sentiment-analysis-in-csv-format).

The purpose of the software in the *IPython Notebooks* of this repository is the deployment and usage of the natural language processing algorithms. It has been developed by Madison MK and itis distributed under a [BSD](/LICENSE.md) license.

## Contact and Support
If you have questions about how to use the content of this repository or need more information about the work of the Data Science team that created it or about [Madison MK](https://madisonmk.com/), you can [contact us](mailto:datascience@madisonmk.com).