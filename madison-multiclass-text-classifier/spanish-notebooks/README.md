![Madison](/img/madison-logo.png) 

# MADISON DATA - Clasificador de Texto en Múltiples Categorías (Sentimiento y Tópicos)

*(English version [here](/madison-multiclass-text-classifier/english-notebooks/README.md))*

## Introducción  
El área de procesamiento de lenguaje natural del equipo de **Madison Data** ha desarrollado un algoritmo clasificador de textos multiclase y lo ha empaquetado para que pueda ser desplegado en la infraestructura que proporciona [AWS Sagemaker](https://aws.amazon.com/es/sagemaker/). 

Este algoritmo modela la clasificación de ***feedback* de experiencia de cliente** en español ya que es uno de los principales áreas de especialidad de Madison MK. Sin embargo, el algoritmo puede ser entrenado con éxito para clasificar textos de otros dominios y en otros idiomas como demuestran los conjuntos de datos de ejemplo que se incluyen.

## Cómo se utiliza
La integración ha sido diseñada para que pueda ser utilizada por equipos de desarrollo sin conocimientos avanzados de Ciencia de Datos o Aprendizaje Automático.

 Tan sólo es necesario suscribirse desde la página del producto [Clasificador de Textos Multiclase de Madison](https://aws.amazon.com/marketplace/pp/prodview-ywbow7v3bdcvo?ref_=srh_res_product_title) en la sección de Algoritmos y Modelos de [AWS Marketplace](https://aws.amazon.com/marketplace/)
 
 Una vez completada la suscripción el producto puede utilizarse siguiendo los pasos que se  detallan en los *notebooks* que se incluyen en el repositorio para dos casos de uso:

   - [Clasificación de textos en categorías](</madison-multiclass-text-classifier/spanish-notebooks/news/Algorithm Test - Madison Multiclass Text Classifier - News Classification.ipynb?viewer=nbviewer>): El *notebook* proporcionado contiene los pasos para completar el entrenamiento de un modelo de clasificiación de textos en categorías temáticas. En el ejemplo se utiliza el conjunto de datos de noticias descrito en la sección siguiente.
   - [Extracción del sentimiento implícito en el texto](</madison-multiclass-text-classifier/spanish-notebooks/imdb-sentiment/Algorithm Test - Madison Multiclass Text Classifier - Sentiment.ipynb?viewer=nbviewer>) El *notebook* proporcionado contiene los pasos para completar el entrenamiento de un modelo que extrae el sentimiento implícito a un texto. En el ejemplo se utiliza el conjunto de datos de reseñas de películas descrito en la sección siguiente.

En ambos casos de usos el resultado obtenido es un modelo entrenado para proporcionar predicciones y que se despliega en un *end-point* que puede ser consumido por cualquier otro software a través de peticiones *REST*.

## Conjuntos de datos
Para facilitar el despliegue proporcionamos dos conjuntos de datos como ejemplo de dos posibles casos de uso. Uno con **noticias en español** y otro con **reseñas de películas en inglés** extraídas de IMDB.

- [Noticias en español](/data/madison-multiclass-text-classifier/news): El conjunto de datos contiene noticias publicadas en el blog Madison MK.
- [Reseñas de películas de IMDB](https://www.kaggle.com/columbine/imdb-dataset-sentiment-analysis-in-csv-format):  El conjunto de datos está repartido en 3 ficheros (Entrenamiento, Test y Validación) con un total de más de 49.000 reseñas que pueden estar clasificadas con sentimiento positivo ('1') o sentimiento negativo ('0')

Sin embargo os animamos a utilizar vuestros propios conjuntos de datos para entrenar el algoritmo y generar modelos completamente personalizados a vuestros casos de uso.

## Licencias
El conjunto de datos de noticias en español se distribuye con [licencia CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.es) y ha sido generado por Madison MK.

El conjunto de datos de reseñas de películas de IMDB, se distribuye con los [términos de uso de los conjuntos de datos del Banco Mundial](https://www.worldbank.org/en/about/legal/terms-of-use-for-datasets) y lo hemos obtenido de una [competición de Kaggle sobre análisis de sentimiento](https://www.kaggle.com/columbine/imdb-dataset-sentiment-analysis-in-csv-format).

El software contenido en los *IPython Notebooks* de este repositorio para desplegar y utilizar los algoritmos de procesamiento del lenguaje natural ha sido creado por Madison MK se distribuye con licencia [BSD](/LICENSE.md).

## Contacto
Si tienes dudas sobre cómo utilizar el contenido de este repositorio o necesitas más información sobre el trabajo del equipo de Ciencia de Datos que lo ha creado o sobre [Madison MK](https://madisonmk.com/), puedes contactar con [nosotros](mailto:datascience@madisonmk.com).